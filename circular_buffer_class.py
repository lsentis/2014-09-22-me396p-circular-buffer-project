class CircularBuffer:
	def __init__(self):
		self.init()

	def init(self):
		self.buf = []
		self.head = 0
		self.tail = 0
		self.isEmpty = True
		self.size = 10
		for ii in range(self.size):
			self.buf.append('nan') # fill with not a number values
		self.isEmpty = True

	def add(self, item):
		if not self.isEmpty: 
			self.head +=1 # increment after first entry is in
			self.head %= self.size # reset head
			if self.head == self.tail: # logic to increade head
				self.tail += 1
				self.tail %= self.size
		self.buf[self.head] = item # add entry to buffer's head
		self.isEmpty = False # empty logic

	def printCircularBuffer(self):
		index = self.tail # index is moves from tail to head
		while index != self.head:
			print self.buf[index]
			index += 1
			index %= self.size
		print self.buf[self.head] # print last element on head


	def getHead(self):
		return self.buf[self.head]

	def getTail(self):
		return self.buf[self.tail]

	def getIndex(self,index):
		return self.buf[ (index + self.tail) % self.size ]

