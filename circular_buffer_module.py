# global variables
buf = [] # empty list initialization
head = 0
tail = 0
isEmpty = True
size = 10

# init function
def init():
	global size, isEmpty # inform function of global variables
	for ii in range(size):
		buf.append('nan') # fill with not a number values
	isEmpty = True

def add(item):
	global head, tail, isEmpty
	if not isEmpty: 
		head +=1 # increment after first entry is in
		head %= size # reset head
		if head == tail: # logic to increade head
			tail += 1
			tail %= size
	buf[head] = item # add entry to buffer's head
	isEmpty = False # empty logic

# function that prints buffer from head to tail
def printCircularBuffer():
	global head, tail, size
	index = tail # index is moves from tail to head
	while index != head:
		print buf[index]
		index += 1
		index %= size
	print buf[head] # print last element on head

def getHead():
	global head
	return buf[head]

def getTail():
	global tail
	return buf[tail]

def getIndex(index):
	global tail, size
	return buf[ (index + tail) % size ]

# # main function
# init()
# jj = 2
# for ii in range(41): # fills up buffer
# 	add(jj)
# 	jj += 1

# print buf # print out buffer
# printCircularBuffer() # print out circular buffer
# print "buf[3] =", getIndex(3)