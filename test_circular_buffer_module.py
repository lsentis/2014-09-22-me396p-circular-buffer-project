import circular_buffer_module as cb # uses circular buffer as module

print cb.init() # init function
for ii in range(43): # populate circular buffer
	cb.add(ii)

cb.printCircularBuffer() # print out